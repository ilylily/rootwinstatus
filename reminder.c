#include <stdio.h>
#include <stddef.h>

#include "common.h"


size_t rws_reminder(char *out, size_t max) {
	return snprintf(out, max, "TODO: read reminders file");
}

struct rws_part rws_reminder_part = {
	.buffer = NULL,
	.buffer_len = 255,
	.callback = rws_reminder
};
