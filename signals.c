#include <signal.h>

extern volatile sig_atomic_t running; // defined in rootwinstatus.c

void interrupt_handler(int signal) {
	if (signal == SIGINT)
		running = 0;
	// else something bad's happened, but, well...
	// let's burn that bridge when we come to it, eh?
}
