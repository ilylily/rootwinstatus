#include <stdio.h>
#include <stddef.h>

#include "common.h"


size_t rws_mpd_status(char *out, size_t max) {
	return snprintf(out, max, "TODO: mpd status output");
}

struct rws_part rws_mpd_part = {
	.buffer = NULL,
	.buffer_len = 128,
	.callback = rws_mpd_status
};
