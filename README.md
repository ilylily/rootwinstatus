# rootwinstatus

a simple "status bar" that outputs to the X11 root window of the current
display. works with dwm + extrabar patch, plus some code to add another bar
segment on the right-hand side of the bar. assumes segments split on '='
