#include <stddef.h>
#include <time.h>

#include "common.h"


size_t rws_date_time(char *out, size_t max) {
	time_t timep = time(NULL);
	struct tm *tm = localtime(&timep);

	return strftime(out, max, "%A, %d %B %Y [%T %Z]", tm);
}

struct rws_part rws_dt_part = {
	.buffer = NULL,
	.buffer_len = 64,
	.callback = rws_date_time
};
