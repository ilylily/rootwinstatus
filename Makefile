CFLAGS ?= -std=c17 -Wall -Wextra -Werror -pedantic -g

x11_CFLAGS != pkg-config --cflags x11
CFLAGS += ${x11_CFLAGS} -D_POSIX_C_SOURCE=200809L
x11_LDLIBS != pkg-config --libs x11
LDLIBS += ${x11_LDLIBS}

rootwinstatus: *.c

rootwinstatus.c: config.h
config.h:

all: rootwinstatus

clean:
	rm -f rootwinstatus
