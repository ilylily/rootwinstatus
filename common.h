#ifndef RWS_COMMON_H
#define RWS_COMMON_H

/* ## rws_part
 * an rws_part contins a buffer for a string, the size of said buffer,
 * and a callback that writes a string value into the buffer.
 * the callback should return the length of the string written.
 */
struct rws_part {
	char *buffer;
	size_t buffer_len;
	size_t (*callback)(char *, size_t);
};

#endif
