#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <X11/Xlib.h>

#include "common.h"
#include "signals.h"

#include "datetime.h"
#include "reminder.h"
#include "mpdstatus.h"

#define DATE_LEN 64
#define REMINDER_LEN 255
#define MEDIA_LEN 128

// just some config
#define SLEEP_MS 200
#define SPLIT_STR "="
#define PARTS rws_dt_part, rws_reminder_part, rws_mpd_part

static const int SPLIT_STR_LEN = sizeof(SPLIT_STR) - 1;
volatile sig_atomic_t running = 1; // modified in signal.c


int main() {
	struct rws_part parts[] = { PARTS };
	size_t parts_len = sizeof(parts) / sizeof(struct rws_part);

	size_t root_name_len = 0;
	char *root_name;

	Display *dpy;
	int screen;
	Window root;

	struct timespec sleep_ts = { 0, SLEEP_MS * 1000000 };

	size_t out_size;

	struct sigaction interrupt_action;

	interrupt_action.sa_handler = interrupt_handler;
	interrupt_action.sa_flags = 0;
	sigemptyset(&interrupt_action.sa_mask);

	if(sigaction(SIGINT, &interrupt_action, NULL) == -1) {
		perror("failed to set interrupt handler");
		return 1;
	}

	// initialize part buffers and root name buffer
	for (size_t i = 0; i < parts_len; i++) {
		struct rws_part *part = &parts[i];
		part->buffer = malloc(part->buffer_len);
		root_name_len += part->buffer_len + 1;
	}
	root_name = malloc(root_name_len);

	// get our display and root window
	dpy = XOpenDisplay(NULL); // maybe take an arg for this?
	if (!dpy) {
		fprintf(stderr, "couldn't open display '%s'\n",
				XDisplayName(NULL));
		XFree(dpy);
		return 1;
	}
	screen = DefaultScreen(dpy);
	root = RootWindow(dpy, screen);

	// run until signal
	while (running) {
		int offset = 0;

		for (size_t i = 0; i < parts_len; i++) {
			out_size = parts[i].callback(
				parts[i].buffer, parts[i].buffer_len
			);
			if (out_size == 0)
				snprintf(
					parts[i].buffer, parts[i].buffer_len,
					"part %lu error", i
				);

			offset += snprintf(
				root_name + offset,
				root_name_len - 3 - offset,
				"%s" SPLIT_STR,
				parts[i].buffer
			);
		}

		root_name[offset - SPLIT_STR_LEN] = '\0';

		XStoreName(dpy, root, root_name);
		XSync(dpy, False);

		nanosleep(&sleep_ts, NULL); // maybe check for interruptions?
	}

	for (size_t i = 0; i < parts_len; i++) free(parts[i].buffer);

	free(root_name);
	XCloseDisplay(dpy);
	return 0;
}
